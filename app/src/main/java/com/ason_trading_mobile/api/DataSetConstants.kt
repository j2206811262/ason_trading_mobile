package com.ason_trading_mobile.api

class DataSetConstants {
    companion object {
        val TAIWAN_STAOCK_PRICE_MINUTE = "TaiwanStockPriceMinute"
        val TAIWAN_STOCK_PRICE = "TaiwanStockPrice"
        val TAIWAN_STOCK_INFO = "TaiwanStockInfo"
        val TAIWAN_STOCK_PRICE_BID_ASK = "TaiwanStockPriceBidAsk"
    }
}