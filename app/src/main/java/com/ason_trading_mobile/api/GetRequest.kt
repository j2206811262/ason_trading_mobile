package com.ason_trading_mobile.api

import com.ason_trading_mobile.dto.BaseStockInfo
import com.ason_trading_mobile.dto.BaseStockPriceBidAsk
import com.ason_trading_mobile.dto.BaseStockPriceInfo
import com.ason_trading_mobile.dto.BaseTradingRecord
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface GetRequest {
    @GET("/api/v3/data")
    fun getTradingRecordByStockID(@Query("dataset") dataSet: String,
                                  @Query("stock_id") stockId: String,
                                  @Query("date") date: String
    ): Call<BaseTradingRecord>

    @GET("/api/v3/data")
    fun getStockPriceInfo(
        @Query("dataset") dataSet: String,
        @Query("stock_id") stockId: String,
        @Query("date") date: String,
        @Query("end_date") endDate: String
    ): Call<BaseStockPriceInfo>

    @GET("/api/v3/data")
    fun getStockInfo(
        @Query("dataset") dataSet: String,
        @Query("stock_id") stockId: String
    ): Call<BaseStockInfo>


    @GET("/api/v4/data")
    fun getStockPriceBidAsk(
        @Query("dataset") dataSet: String,
        @Query("data_id") stockId: String,
        @Query("start_date") startDate: String
    ): Call<BaseStockPriceBidAsk>
}