package com.ason_trading_mobile.viewholder

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ason_trading_mobile.R

class BidOrAskViewHolder(view: View) : RecyclerView.ViewHolder(view){
    val priceTV: TextView = itemView.findViewById(R.id.tv_price)
    val volumeTV: TextView = itemView.findViewById(R.id.tv_volume)
    val underlineView: View = itemView.findViewById(R.id.view_underline)
}