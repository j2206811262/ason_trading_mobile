package com.ason_trading_mobile.viewholder

import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.ason_trading_mobile.R

class TradingRecordViewHolder(view: View) : RecyclerView.ViewHolder(view) {
    val cellLinearLayout: LinearLayout = itemView.findViewById(R.id.cell_ll)
    val timeTV: TextView = itemView.findViewById(R.id.tv_time)
    val tickTV: TextView = itemView.findViewById(R.id.tv_tick)
    val dealPriceTV: TextView = itemView.findViewById(R.id.deal_price)
    val volumeTV: TextView = itemView.findViewById(R.id.tv_volume)
}