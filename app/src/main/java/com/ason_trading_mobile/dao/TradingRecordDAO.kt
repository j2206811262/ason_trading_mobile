package com.ason_trading_mobile.dao

import androidx.room.*
import com.ason_trading_mobile.model.TradingRecord

@Dao
interface TradingRecordDAO {
    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(tradingRecord: TradingRecord)

    @Update
    suspend fun update(tradingRecord: TradingRecord)

    @Delete
    suspend fun delete(tradingRecord: TradingRecord)

    @Query("DELETE FROM trading_record WHERE stockID = :stockID and date = :date")
    suspend fun deletePreviousData(stockID: String, date: String)

    @Query("SELECT * FROM trading_record WHERE stockId = :stockID and date = :date ORDER BY id ASC")
    suspend fun getTradingRecord(stockID: String, date: String): List<TradingRecord>
}