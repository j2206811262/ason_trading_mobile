package com.ason_trading_mobile.dao

import androidx.room.*
import com.ason_trading_mobile.model.StockTopFiveOrder
import com.ason_trading_mobile.model.StockTopFiveOrderData
import com.ason_trading_mobile.model.TopFiveOrderRelation

@Dao
interface StockTopFiveOrderDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertStockTopFiveOrder(stockTopFiveOrder: StockTopFiveOrder): Long

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insertStockTopFiveOrderData(stockTopFiveOrderData: StockTopFiveOrderData)

    @Delete
    suspend fun delete(stockTopFiveOrder: StockTopFiveOrder)

    @Query("DELETE FROM stock_top_five_order WHERE stockId = :stockID and date = :date")
    suspend fun deletePreviousData(stockID: String, date: String)

    @Transaction
    @Query("SELECT * FROM stock_top_five_order WHERE stockId = :stockId and date = :date ORDER BY id ASC")
    suspend fun getTopFiveOrderRelation(stockId: String, date: String): List<TopFiveOrderRelation>
}