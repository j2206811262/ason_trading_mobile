package com.ason_trading_mobile.constants

class GeneralConstants {
    companion object {
        const val BASE_URL = "https://api.finmindtrade.com"

        const val FORMAT_YYYY_MM_DD = "yyyy-MM-dd"
        const val FORMAT_HH_mm_ss = "HH:mm:ss"

        const val TIME_FORMAT = "[0-9]{2}:[0-9]{2}:[0-9]{2}"

        const val NONE_SIGN = "-"
    }
}