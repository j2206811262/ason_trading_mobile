package com.ason_trading_mobile.utils

import android.content.Context
import android.widget.TextView
import androidx.core.content.ContextCompat
import com.ason_trading_mobile.R
import com.ason_trading_mobile.constants.GeneralConstants
import java.lang.ref.WeakReference
import java.math.BigDecimal
import java.text.SimpleDateFormat
import java.util.*
import java.util.regex.Pattern

class Utility {
    companion object {
        fun appendPrefixZero(value: String, digit: Int): String {
            var valueStr = value
            return if (valueStr.length < digit) {
                for (i in 1 until digit) {
                    valueStr = "0$valueStr"
                }
                valueStr
            } else {
                valueStr
            }
        }

        fun appendPostDigitZero(value: Double, digit: Int): String {
            val valueStr = value.toString()
            return if (valueStr.split(".")[1].length < digit) {
                val integerDigitPart = valueStr.split(".")[0]
                var postDigitPart = valueStr.split(".")[1]
                for (i in 1..(digit - postDigitPart.length)) {
                    postDigitPart += "0"
                }
                "$integerDigitPart.$postDigitPart"
            } else {
                valueStr
            }
        }

        fun appendPostDigitZero(valueStr: String, digit: Int): String {
            if (valueStr.matches(Regex("[0-9]+\\.[0-9]+"))) {
                if (valueStr.split(".")[1].length < digit) {
                    val integerDigitPart = valueStr.split(".")[0]
                    var postDigitPart = valueStr.split(".")[1]
                    for (i in 1..(digit - postDigitPart.length)) {
                        postDigitPart += "0"
                    }
                    return "$integerDigitPart.$postDigitPart"
                } else {
                    return valueStr
                }
            }
            return valueStr
        }

        fun subtract(d1: Double, d2: Double): Double {
            val bd1 = BigDecimal(java.lang.Double.toString(d1))
            val bd2 = BigDecimal(java.lang.Double.toString(d2))
            return bd1.subtract(bd2).setScale(2).toDouble()
        }

        fun getDaysAgo(daysAgo: Int): String {
            val calendar = Calendar.getInstance()

            while (calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SATURDAY
                || calendar.get(Calendar.DAY_OF_WEEK) == Calendar.SUNDAY
            ) {
                calendar.add(Calendar.DAY_OF_YEAR, -1)
            }

            calendar.add(Calendar.DAY_OF_YEAR, -daysAgo)
            return SimpleDateFormat(GeneralConstants.FORMAT_YYYY_MM_DD, Locale.TAIWAN)
                .format(calendar.time)
        }


        fun getTimeSince9AM(secondOffset: Int): String {
            if (secondOffset < 60) {
                return "09:00:" + appendPrefixZero(secondOffset.toString(), 2)
            } else if (secondOffset < 3600) {
                val minutes = secondOffset / 60
                val second = secondOffset % 60;
                return "09:" + appendPrefixZero(minutes.toString(), 2) + ":" +
                        appendPrefixZero(second.toString(), 2)
            } else {
                val hours = secondOffset / 3600
                val minutes = (secondOffset % 3600) / 60
                val second = secondOffset % 3600 % 60;
                return appendPrefixZero((9 + hours).toString(), 2) + ":" +
                        appendPrefixZero(minutes.toString(), 2) + ":" +
                        appendPrefixZero(second.toString(), 2)
            }
        }

        fun compareTimeLessOrEqualThan(time1: String, time2: String): Boolean {
            return if (time1.matches(Regex(GeneralConstants.TIME_FORMAT))
                && time2.matches(Regex(GeneralConstants.TIME_FORMAT))
            ) {
                val stringFormat = SimpleDateFormat(GeneralConstants.FORMAT_HH_mm_ss, Locale.TAIWAN)
                val timeDateFormat1 = stringFormat.parse(time1)
                val timeDateFormat2 = stringFormat.parse(time2)
                (timeDateFormat1!!.time - timeDateFormat2!!.time <= 0)
            } else {
                false
            }

        }


        fun drawColorByTickType(
            textView: TextView,
            tickType: Int,
            context: WeakReference<Context>
        ) {
            if (context.get() != null) {
                if (tickType == 2) {
                    textView.setTextColor(
                        ContextCompat.getColor(
                            context.get()!!,
                            R.color.light_green
                        )
                    )
                } else {
                    textView.setTextColor(
                        ContextCompat.getColor(
                            context.get()!!,
                            R.color.light_red
                        )
                    )
                }
            }
        }

        fun drawColorByTick(textView: TextView, tick: Double, context: WeakReference<Context>) {
            if (context.get() != null) {
                when {
                    tick < 0 -> {
                        textView.setTextColor(
                            ContextCompat.getColor(
                                context.get()!!,
                                R.color.light_green
                            )
                        )
                    }
                    tick == 0.0 -> {
                        textView.setTextColor(
                            ContextCompat.getColor(
                                context.get()!!,
                                R.color.yellow
                            )
                        )
                    }
                    else -> {
                        textView.setTextColor(
                            ContextCompat.getColor(
                                context.get()!!,
                                R.color.light_red
                            )
                        )
                    }
                }
            }
        }

        fun convertValueToTickFormat(tick: Double): String {
            return when {
                tick > 0 -> {
                    "+" + appendPostDigitZero(tick, 2)
                }
                tick < 0 -> {
                    "-" + appendPostDigitZero(tick, 2)
                }
                else -> {
                    appendPostDigitZero(tick.toString(), 2)
                }
            }
        }

        fun parsePattern(input: String, format: String): String {
            val pattern = Pattern.compile(format)
            val matcher = pattern.matcher(input)

            if (matcher.find()) {
                return matcher.group() ?: ""
            }
            return ""
        }
    }
}