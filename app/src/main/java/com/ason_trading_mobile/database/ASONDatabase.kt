package com.ason_trading_mobile.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.ason_trading_mobile.dao.StockTopFiveOrderDAO
import com.ason_trading_mobile.dao.TradingRecordDAO
import com.ason_trading_mobile.model.StockTopFiveOrder
import com.ason_trading_mobile.model.StockTopFiveOrderData
import com.ason_trading_mobile.model.TradingRecord

@Database(
    entities = [TradingRecord::class, StockTopFiveOrder::class, StockTopFiveOrderData::class]
    , version = 1
)
abstract class ASONDatabase : RoomDatabase(){
    companion object {
        @Volatile private var asonDatabase: ASONDatabase? = null

        fun getInstance(context: Context) : ASONDatabase =
            asonDatabase ?: synchronized(this) {
                asonDatabase ?: buildDatabase(context).also {
                    asonDatabase = it
                }
            }


        private fun buildDatabase(context: Context): ASONDatabase =
            Room.databaseBuilder(
                context.applicationContext,
                ASONDatabase::class.java,
                "asonDatabase"
            ).fallbackToDestructiveMigration().build()

    }

    abstract fun tradingRecordDAO() : TradingRecordDAO
    abstract fun stockBidAskDAO(): StockTopFiveOrderDAO
}