package com.ason_trading_mobile.fragment

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import com.ason_trading_mobile.R
import com.ason_trading_mobile.activity.MainActivity
import com.ason_trading_mobile.adapter.BidOrAskAdapter
import com.ason_trading_mobile.adapter.TradingRecordAdapter
import com.ason_trading_mobile.databinding.FragMainPageBinding
import com.ason_trading_mobile.dto.StockPriceBidAsk
import com.ason_trading_mobile.model.PriceVolumeObject
import com.ason_trading_mobile.model.TradingRecord
import com.ason_trading_mobile.utils.Utility
import com.ason_trading_mobile.viewmodel.MainPageViewModel
import com.ason_trading_mobile.viewmodel.ViewModelFactory
import java.lang.ref.WeakReference

class MainPageFrag : BaseFragment() {
    private val TAG = "MainPageFrag.kt"
    private val tradingRecordAdapter =  TradingRecordAdapter()
    private val askAdapter = BidOrAskAdapter(false) //委賣
    private val bidAdapter = BidOrAskAdapter(true) //委買

    private lateinit var viewModel: MainPageViewModel
    private lateinit var binding: FragMainPageBinding

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        binding = DataBindingUtil
            .inflate(inflater, R.layout.frag_main_page, container, false)
        initViewModel()

        binding.lifecycleOwner = this
        binding.viewModel = viewModel

        binding.rvTradingRecord.layoutManager = LinearLayoutManager(context)
        binding.rvTradingRecord.setHasFixedSize(true)
        binding.rvTradingRecord.adapter = tradingRecordAdapter

        binding.rvAsk.layoutManager = LinearLayoutManager(context)
        binding.rvAsk.setHasFixedSize(true)
        binding.rvAsk.adapter = askAdapter

        binding.rvBid.layoutManager = LinearLayoutManager(context)
        binding.rvBid.setHasFixedSize(true)
        binding.rvBid.adapter = bidAdapter

        return binding.root
    }

    private fun initViewModel() {
        viewModel = ViewModelProvider(this, ViewModelFactory(activity!!.application))
            .get(MainPageViewModel::class.java)

        viewModel.toastMessage.observe(this, toastMessageObserver)
        viewModel.isViewLoading.observe(this, isViewLoadingObserver)
        viewModel.displayTradingRecordList.observe(this, displayTradingRecordObserver)
        viewModel.stockBidAsk.observe(this, stockBidAskObserver)
    }

    private val isViewLoadingObserver = Observer<Boolean> {
        (activity as MainActivity).showProgressBar(it)
    }

    private val toastMessageObserver = Observer<String> {
        if (it != null) {
            Toast.makeText(context, it, Toast.LENGTH_SHORT).show()
        }
    }

    private val stockBidAskObserver = Observer<StockPriceBidAsk> {
        if (it != null) {
            val tempAskArrayList = arrayListOf<PriceVolumeObject>()
            val tempBidArrayList = arrayListOf<PriceVolumeObject>()
            for (index in it.AskPrice.indices) {
                tempAskArrayList.add(
                    PriceVolumeObject(it.AskPrice[index], it.AskVolume[index])
                )
            }
            for (index in it.BidPrice.indices) {
                tempBidArrayList.add(
                    PriceVolumeObject(it.BidPrice[index], it.BidVolume[index])
                )
            }
            askAdapter.refresh(
                tempAskArrayList,
                it.yesterdayClosePrice,
                viewModel.stockCurrentPrice.value
            )
            bidAdapter.refresh(
                tempBidArrayList,
                it.yesterdayClosePrice,
                viewModel.stockCurrentPrice.value
            )
        }
    }

    private val displayTradingRecordObserver = Observer<List<TradingRecord>> {
        Log.d(TAG, "trading record update size: " + it.size)
        tradingRecordAdapter.refresh(it)
        if (it.isNotEmpty()) {
            binding.tvStockCurrentPrice.text = Utility.appendPostDigitZero(it[0].dealPrice, 2)
            binding.tvStockCurrentTick.text = Utility.convertValueToTickFormat(it[0].tick)
            Utility.drawColorByTick(
                binding.tvStockCurrentPrice,
                it[0].tick,
                WeakReference<Context>(context)
            )
            Utility.drawColorByTick(
                binding.tvStockCurrentTick,
                it[0].tick,
                WeakReference<Context>(context)
            )
        }

    }
}