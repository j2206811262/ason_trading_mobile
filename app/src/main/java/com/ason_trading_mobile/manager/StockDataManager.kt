package com.ason_trading_mobile.manager

import android.app.Application
import com.ason_trading_mobile.database.ASONDatabase
import com.ason_trading_mobile.dto.StockPriceBidAsk
import com.ason_trading_mobile.model.StockTopFiveOrder
import com.ason_trading_mobile.model.StockTopFiveOrderData
import com.ason_trading_mobile.model.TopFiveOrderRelation
import com.ason_trading_mobile.model.TradingRecord
import com.ason_trading_mobile.repository.StockBidAskRepository
import com.ason_trading_mobile.repository.TradingRecordRepository
import com.ason_trading_mobile.utils.Utility

class StockDataManager(application: Application) {
    private val tradingRecordRepository: TradingRecordRepository
    private val stockBidAskRepository: StockBidAskRepository

    companion object {
        private const val TAG = "StockDataManager.kt"

        @Volatile
        private var instance: StockDataManager? = null

        fun getInstance(application: Application): StockDataManager =
            instance ?: synchronized(this) {
                instance ?: StockDataManager(application).also { instance = it }
            }
    }

    init {
        val tradingRecordDAO = ASONDatabase.getInstance(application).tradingRecordDAO()
        val stockBidAskDAO = ASONDatabase.getInstance(application).stockBidAskDAO()
        tradingRecordRepository = TradingRecordRepository(tradingRecordDAO)
        stockBidAskRepository = StockBidAskRepository(stockBidAskDAO)
    }

    suspend fun saveTradingRecords(tradingRecords: ArrayList<TradingRecord>) {
        if (tradingRecords.size > 0) {
            val temp = tradingRecords[0]
            tradingRecordRepository.deletePreviousData(temp.stockId, temp.date)
            for (data in tradingRecords) {
                tradingRecordRepository.insert(data)
            }
        }
    }

    suspend fun saveStockTopFiveOrderData(stockTopFiveOrderList: ArrayList<StockPriceBidAsk>) {
        if (stockTopFiveOrderList.size > 0) {
            val temp = stockTopFiveOrderList[0]
            stockBidAskRepository.deletePreviousData(temp.stock_id, temp.date)
            for (data in stockTopFiveOrderList) {
                val returnID = stockBidAskRepository.insertStockTopFiveOrder(
                    StockTopFiveOrder(
                        0,
                        data.Time,
                        data.date,
                        data.stock_id,
                        data.yesterdayClosePrice
                    )
                )
                for (orderData in data.AskPrice) {
                    stockBidAskRepository.insertStockTopFiveOrderData(
                        StockTopFiveOrderData(
                            0,
                            returnID.toInt(),
                            orderData,
                            isBidData = false,
                            isPriceData = true
                        )
                    )
                }
                for (orderData in data.AskVolume) {
                    stockBidAskRepository.insertStockTopFiveOrderData(
                        StockTopFiveOrderData(
                            0,
                            returnID.toInt(),
                            orderData.toDouble(),
                            isBidData = false,
                            isPriceData = false
                        )
                    )
                }
                for (orderData in data.BidPrice) {
                    stockBidAskRepository.insertStockTopFiveOrderData(
                        StockTopFiveOrderData(
                            0,
                            returnID.toInt(),
                            orderData,
                            isBidData = true,
                            isPriceData = true
                        )
                    )
                }

                for (orderData in data.BidVolume) {
                    stockBidAskRepository.insertStockTopFiveOrderData(
                        StockTopFiveOrderData(
                            0,
                            returnID.toInt(),
                            orderData.toDouble(),
                            isBidData = true,
                            isPriceData = false
                        )
                    )
                }
            }
        }
    }

    suspend fun getTradingRecords(stockID: String): ArrayList<TradingRecord> {
        val downloadDate = Utility.getDaysAgo(0)
        val previousDate = Utility.getDaysAgo(1)

        return ArrayList(tradingRecordRepository.getTradingRecord(stockID, downloadDate))
    }

    suspend fun getTopFiveOrderRelation(stockID: String): ArrayList<TopFiveOrderRelation> {
        val downloadDate = Utility.getDaysAgo(0)

        return ArrayList(stockBidAskRepository.getTopFiveOrderRelation(stockID, downloadDate))
    }
}