package com.ason_trading_mobile.manager

import android.util.Log
import com.ason_trading_mobile.api.DataSetConstants
import com.ason_trading_mobile.api.GetRequest
import com.ason_trading_mobile.api.RetrofitClient
import com.ason_trading_mobile.constants.GeneralConstants
import com.ason_trading_mobile.dto.BaseStockPriceBidAsk
import com.ason_trading_mobile.dto.BaseTradingRecord
import com.ason_trading_mobile.dto.StockPriceBidAsk
import com.ason_trading_mobile.model.TradingRecord
import com.ason_trading_mobile.utils.Utility
import retrofit2.awaitResponse

class DownloadManager {
    companion object {
        private const val TAG = "DownloadManager.kt"
    }

    suspend fun downloadTradingRecord(stockID: String, yesterdayClosePrice: Double)
            : ArrayList<TradingRecord> {
        val result = arrayListOf<TradingRecord>()
        val request = RetrofitClient.createRetrofit(GeneralConstants.BASE_URL)
            .create(GetRequest::class.java)
        val downloadedDate = Utility.getDaysAgo(0)

        val response = request.getTradingRecordByStockID(
            DataSetConstants.TAIWAN_STAOCK_PRICE_MINUTE
            , stockID, downloadedDate
        ).awaitResponse()
        Log.d(TAG, "Response Code: ${response.code()}")
        if (response.isSuccessful) {
            Log.d(TAG, "Download Successfully!")
            if (response.body() == null) {
                Log.d(TAG, "Download empty body!")
            } else {
                val myData: BaseTradingRecord = response.body()!!
                for (apiTradingRecord in myData.apiTradingRecord) {
                    //only retrieve data after 9:00:00
                    if (!apiTradingRecord.Time.matches(Regex("08:[0-9]+:[0-9]+\\.[0-9]+"))) {
                        val temp = TradingRecord(
                            0
                            , downloadedDate
                            , apiTradingRecord.Time.split(".")[0]
                            , stockID
                            , apiTradingRecord.deal_price
                            , apiTradingRecord.volume
                            , Utility.subtract(
                                apiTradingRecord.deal_price,
                                yesterdayClosePrice
                            )
                            , apiTradingRecord.TickType.toInt()
                        )
                        result.add(temp)
                    }
                }
            }
        }
        return result
    }

    suspend fun downloadStockBidAsk(stockID: String, yesterdayClosePrice: Double)
            : ArrayList<StockPriceBidAsk> {
        val tempArrayList = arrayListOf<StockPriceBidAsk>()
        val request = RetrofitClient.createRetrofit(GeneralConstants.BASE_URL)
            .create(GetRequest::class.java)

        val response = request.getStockPriceBidAsk(
            DataSetConstants.TAIWAN_STOCK_PRICE_BID_ASK
            , stockID, Utility.getDaysAgo(0)
        ).awaitResponse()
        Log.d(TAG, "Response Code: ${response.code()}")
        if (response.isSuccessful) {
            val dataPerSecond = HashMap<String, String>()
            Log.d(TAG, "Download Successfully!")
            if (response.body() == null) {
                Log.d(TAG, "Download empty body!")
            } else {
                val myData: BaseStockPriceBidAsk = response.body()!!

                for (stockPriceBidAsk in myData.taiwanStockPriceBidAsk) {
                    //only retrieve data after 9:00:00
                    if (!stockPriceBidAsk.Time.matches(Regex("08:[0-9]+:[0-9]+\\.[0-9]+"))) {
                        val hourMinutesKey = Utility.parsePattern(
                            stockPriceBidAsk.Time
                            , GeneralConstants.TIME_FORMAT
                        )
                        if (!dataPerSecond.containsKey(hourMinutesKey)) {
                            Log.e(TAG, stockPriceBidAsk.Time)
                            dataPerSecond.put(hourMinutesKey, "hello")
                            val stockBidAsk = StockPriceBidAsk(
                                stockPriceBidAsk.AskPrice,
                                stockPriceBidAsk.AskVolume,
                                stockPriceBidAsk.BidPrice,
                                stockPriceBidAsk.BidVolume,
                                stockPriceBidAsk.Time.split(".")[0],
                                stockPriceBidAsk.date,
                                stockPriceBidAsk.stock_id,
                                yesterdayClosePrice
                            )
                            tempArrayList.add(stockBidAsk)
                        }
                    }
                }
            }
        }
        return tempArrayList
    }
}