package com.ason_trading_mobile.dto

data class APITradingRecord(
    val TickType: Double,
    val Time: String,
    val date: String,
    val deal_price: Double,
    val stock_id: String,
    val volume: Int
)