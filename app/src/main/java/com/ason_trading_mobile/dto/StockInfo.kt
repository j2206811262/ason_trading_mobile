package com.ason_trading_mobile.dto

data class StockInfo(
    val industry_category: String,
    val stock_id: String,
    val stock_name: String,
    val type: String
)