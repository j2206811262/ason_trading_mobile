package com.ason_trading_mobile.dto

import com.google.gson.annotations.SerializedName

data class BaseStockInfo(
    @SerializedName("data")
    val stockInfo: List<StockInfo>,
    val msg: String,
    val status: Int
)