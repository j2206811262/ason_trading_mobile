package com.ason_trading_mobile.dto

data class StockPriceInfo(
    val Trading_Volume: Long,
    val Trading_money: Long,
    val Trading_turnover: Int,
    val close: Double,
    val date: String,
    val max: Double,
    val min: Double,
    val `open`: Double,
    val spread: Double,
    val stock_id: String
)