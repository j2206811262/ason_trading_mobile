package com.ason_trading_mobile.dto

data class StockPriceBidAsk(
    val AskPrice: List<Double>,
    val AskVolume: List<Int>,
    val BidPrice: List<Double>,
    val BidVolume: List<Int>,
    val Time: String,
    val date: String,
    val stock_id: String,
    val yesterdayClosePrice: Double
)