package com.ason_trading_mobile.dto

import com.google.gson.annotations.SerializedName

data class BaseTradingRecord(
    @SerializedName("data")
    val apiTradingRecord: List<APITradingRecord>,
    val msg: String,
    val status: Int
)