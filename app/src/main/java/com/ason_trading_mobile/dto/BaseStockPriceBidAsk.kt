package com.ason_trading_mobile.dto

import com.google.gson.annotations.SerializedName

data class BaseStockPriceBidAsk(
    @SerializedName("data")
    val taiwanStockPriceBidAsk: List<StockPriceBidAsk>,
    val msg: String,
    val status: Int
)