package com.ason_trading_mobile.dto

import com.google.gson.annotations.SerializedName

data class BaseStockPriceInfo(
    @SerializedName("data")
    val stockPriceInfo: List<StockPriceInfo>,
    val msg: String,
    val status: Int
)