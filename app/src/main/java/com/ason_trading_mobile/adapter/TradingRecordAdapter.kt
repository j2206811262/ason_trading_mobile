package com.ason_trading_mobile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.ason_trading_mobile.R
import com.ason_trading_mobile.model.TradingRecord
import com.ason_trading_mobile.utils.Utility
import com.ason_trading_mobile.viewholder.TradingRecordViewHolder
import java.lang.ref.WeakReference

class TradingRecordAdapter : RecyclerView.Adapter<TradingRecordViewHolder>(){
    private var tradingRecordList: List<TradingRecord> = emptyList()
    private lateinit var context : Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TradingRecordViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(
            R.layout.cell_realtime_trading_record,
            parent,
            false
        )
        context = parent.context

        return TradingRecordViewHolder(view)
    }

    override fun getItemCount(): Int {
        return tradingRecordList.size
    }

    override fun onBindViewHolder(holder: TradingRecordViewHolder, position: Int) {
        val tradingRecord = tradingRecordList[position]
        holder.timeTV.text = tradingRecord.time
        holder.dealPriceTV.text = Utility.appendPostDigitZero(tradingRecord.dealPrice, 2)
        holder.volumeTV.text = tradingRecord.volume.toString()
        holder.tickTV.text = Utility.convertValueToTickFormat(tradingRecord.tick)

        if (position % 2 == 0) {
            holder.cellLinearLayout.setBackgroundColor(
                ContextCompat.getColor(
                    context,
                    R.color.dark_gray
                )
            )
        } else {
            holder.cellLinearLayout.setBackgroundColor(ContextCompat.getColor(context, R.color.black))
        }


//        Utility.drawColorByTick(holder.timeTV, tradingRecord.tick, WeakReference(context))
        Utility.drawColorByTick(holder.dealPriceTV, tradingRecord.tick, WeakReference(context))
        Utility.drawColorByTickType(holder.volumeTV, tradingRecord.tickType, WeakReference(context))
        Utility.drawColorByTick(holder.tickTV, tradingRecord.tick, WeakReference(context))
    }

    fun refresh(it: List<TradingRecord>) {
        tradingRecordList = it
        notifyDataSetChanged()
    }
}