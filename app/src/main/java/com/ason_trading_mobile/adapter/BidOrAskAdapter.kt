package com.ason_trading_mobile.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.ason_trading_mobile.R
import com.ason_trading_mobile.constants.GeneralConstants
import com.ason_trading_mobile.model.PriceVolumeObject
import com.ason_trading_mobile.utils.Utility
import com.ason_trading_mobile.viewholder.BidOrAskViewHolder
import org.apache.commons.lang3.StringUtils
import java.lang.ref.WeakReference

class BidOrAskAdapter(private val isLeftPart: Boolean) : RecyclerView.Adapter<BidOrAskViewHolder>() {
    private var priceVolumeArrayList = arrayListOf<PriceVolumeObject>()
    private var yesterdayPrice = 0.0
    private var stockCurrentPrice = ""
    private lateinit var context : Context

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BidOrAskViewHolder {
        val viewID = if (isLeftPart) {
            R.layout.cell_bid_or_ask_left_part
        } else {
            R.layout.cell_bid_or_ask_right_part
        }

        val view = LayoutInflater.from(parent.context).inflate(
            viewID,
            parent,
            false
        )
        context = parent.context

        return BidOrAskViewHolder(view)
    }

    override fun getItemCount(): Int {
        return priceVolumeArrayList.size
    }

    override fun onBindViewHolder(holder: BidOrAskViewHolder, position: Int) {
        val priceVolumeObject = priceVolumeArrayList[position]
        var tick = 0.0
        if (priceVolumeObject.price == 0.0) {
            holder.priceTV.text = GeneralConstants.NONE_SIGN
            holder.volumeTV.text = GeneralConstants.NONE_SIGN
        } else {
            tick = priceVolumeObject.price - yesterdayPrice
            holder.priceTV.text = Utility.appendPostDigitZero(priceVolumeObject.price.toString(), 2)
            holder.volumeTV.text = priceVolumeObject.volume.toString()
        }
        Utility.drawColorByTick(holder.priceTV, tick, WeakReference(context))

        if (StringUtils.equals(holder.priceTV.text, stockCurrentPrice)) {
            //draw underline
            holder.underlineView.visibility = View.VISIBLE
        } else {
            holder.underlineView.visibility = View.INVISIBLE
        }
    }

    fun refresh(
        priceVolumeArrayList: ArrayList<PriceVolumeObject>,
        yesterdayPrice: Double,
        stockCurrentPrice: String?
    ) {
        this.priceVolumeArrayList = priceVolumeArrayList
        this.yesterdayPrice = yesterdayPrice
        this.stockCurrentPrice = stockCurrentPrice ?: "0"
        notifyDataSetChanged()
    }
}