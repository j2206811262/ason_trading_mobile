package com.ason_trading_mobile.activity

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.WindowManager
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.ason_trading_mobile.databinding.ActivityMainBinding
import com.ason_trading_mobile.fragment.MainPageFrag
import com.ason_trading_mobile.viewmodel.MainActivityViewModel
import com.ason_trading_mobile.viewmodel.ViewModelFactory

class MainActivity : BaseActivity() {
    private lateinit var viewModel: MainActivityViewModel
    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        initViewModel()
        binding = ActivityMainBinding.inflate(LayoutInflater.from(this))
        binding.viewModel = viewModel
        binding.lifecycleOwner = this
        setContentView(binding.root)

        navigateTo(MainPageFrag(), false)

    }

    private fun initViewModel() {
        viewModel =
            ViewModelProvider(
                this,
                ViewModelFactory(this.application)
            ).get(MainActivityViewModel::class.java)

        viewModel.isViewLoading.observe(this, isViewLoadingObserver)
    }


    private val isViewLoadingObserver = Observer<Boolean> {
        if (it) {
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
            binding.progressBar.visibility = View.VISIBLE
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE);
            binding.progressBar.visibility = View.GONE
        }
    }


    fun showProgressBar(show: Boolean) {
        viewModel.isViewLoading.postValue(show)
    }

}