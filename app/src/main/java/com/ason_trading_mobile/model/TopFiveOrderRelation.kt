package com.ason_trading_mobile.model

import androidx.room.Embedded
import androidx.room.Relation

data class TopFiveOrderRelation(
    @Embedded
    val stockTopFiveOrder: StockTopFiveOrder,
    @Relation(
        parentColumn = "id",
        entityColumn = "stockTopFiveOrderFK"
    )
    val topFiveOrderData: List<StockTopFiveOrderData>
)