package com.ason_trading_mobile.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "trading_record")
data class TradingRecord (@PrimaryKey(autoGenerate = true) val id: Int
                          , val date: String,
                          val time: String,
                          val stockId: String,
                          val dealPrice: Double,
                          val volume: Int,
                          val tick: Double,
                          val tickType: Int
)