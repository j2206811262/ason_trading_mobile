package com.ason_trading_mobile.model

data class PriceVolumeObject (
    val price: Double,
    val volume: Int
)