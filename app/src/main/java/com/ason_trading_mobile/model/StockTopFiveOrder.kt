package com.ason_trading_mobile.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "stock_top_five_order")
data class StockTopFiveOrder(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val Time: String,
    val date: String,
    val stockId: String,
    val yesterdayPrice: Double
)

