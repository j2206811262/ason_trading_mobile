package com.ason_trading_mobile.model

import androidx.room.Entity
import androidx.room.PrimaryKey


@Entity(
    tableName = "stock_top_five_order_data"
)
data class StockTopFiveOrderData(
    @PrimaryKey(autoGenerate = true)
    val id: Int,
    val stockTopFiveOrderFK: Int,
    val value: Double,
    val isBidData: Boolean,
    val isPriceData: Boolean
)