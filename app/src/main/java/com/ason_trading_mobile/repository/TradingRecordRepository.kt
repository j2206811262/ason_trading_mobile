package com.ason_trading_mobile.repository

import com.ason_trading_mobile.dao.TradingRecordDAO
import com.ason_trading_mobile.model.TradingRecord

class TradingRecordRepository(private val tradingRecordDAO: TradingRecordDAO) {

    suspend fun getTradingRecord(stockID: String, date: String): List<TradingRecord> {
        return tradingRecordDAO.getTradingRecord(stockID, date)
    }

    suspend fun insert(tradingRecord: TradingRecord) {
        tradingRecordDAO.insert(tradingRecord)
    }

    suspend fun update(tradingRecord: TradingRecord) {
        tradingRecordDAO.update(tradingRecord)
    }

    suspend fun delete(tradingRecord: TradingRecord) {
        tradingRecordDAO.delete(tradingRecord)
    }

    suspend fun deletePreviousData(stockID: String, date: String) {
        tradingRecordDAO.deletePreviousData(stockID, date)
    }
}