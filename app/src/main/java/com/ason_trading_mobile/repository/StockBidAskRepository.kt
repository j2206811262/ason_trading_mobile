package com.ason_trading_mobile.repository

import com.ason_trading_mobile.dao.StockTopFiveOrderDAO
import com.ason_trading_mobile.model.StockTopFiveOrder
import com.ason_trading_mobile.model.StockTopFiveOrderData
import com.ason_trading_mobile.model.TopFiveOrderRelation

class StockBidAskRepository(private val stockTopFiveOrderDAO: StockTopFiveOrderDAO) {

    suspend fun getTopFiveOrderRelation(stockID: String, date: String): List<TopFiveOrderRelation> {
        return stockTopFiveOrderDAO.getTopFiveOrderRelation(stockID, date)
    }

    suspend fun insertStockTopFiveOrder(stockTopFiveOrder: StockTopFiveOrder): Long {
        return stockTopFiveOrderDAO.insertStockTopFiveOrder(stockTopFiveOrder)
    }

    suspend fun insertStockTopFiveOrderData(stockTopFiveOrderData: StockTopFiveOrderData) {
        stockTopFiveOrderDAO.insertStockTopFiveOrderData(stockTopFiveOrderData)
    }

    suspend fun delete(stockBidAsk: StockTopFiveOrder) {
        stockTopFiveOrderDAO.delete(stockBidAsk)
    }

    suspend fun deletePreviousData(stockID: String, date: String) {
        stockTopFiveOrderDAO.deletePreviousData(stockID, date)
    }
}