package com.ason_trading_mobile.viewmodel

import android.app.Application
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.ason_trading_mobile.api.DataSetConstants
import com.ason_trading_mobile.api.GetRequest
import com.ason_trading_mobile.api.RetrofitClient
import com.ason_trading_mobile.constants.GeneralConstants
import com.ason_trading_mobile.dto.BaseStockInfo
import com.ason_trading_mobile.dto.BaseStockPriceInfo
import com.ason_trading_mobile.dto.StockPriceBidAsk
import com.ason_trading_mobile.manager.DownloadManager
import com.ason_trading_mobile.manager.StockDataManager
import com.ason_trading_mobile.model.TradingRecord
import com.ason_trading_mobile.utils.Utility
import io.reactivex.Observable
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.schedulers.Schedulers
import kotlinx.coroutines.launch
import retrofit2.awaitResponse
import java.util.*
import java.util.concurrent.TimeUnit
import kotlin.collections.ArrayList

class MainPageViewModel(application: Application) : AndroidViewModel(application) {
    val displayTradingRecordList = MutableLiveData<ArrayList<TradingRecord>>()
    val stockBidAsk = MutableLiveData<StockPriceBidAsk>()
    val stockId = MutableLiveData<String>()
    val stockInfoID = MutableLiveData<String>()
    val stockInfoName = MutableLiveData<String>()
    val stockCurrentPrice = MutableLiveData<String>()
    val stockCurrentTick = MutableLiveData<String>()

    val toastMessage = MutableLiveData<String>()
    val isViewLoading = MutableLiveData<Boolean>()

    private var tempBidAsk = ArrayList<StockPriceBidAsk>()
    private var tempTradingRecord = ArrayList<TradingRecord>()

    private var tradingRecordPolling: Disposable? = null
    private var stockPriceBidAskPolling: Disposable? = null
    private val TAG = "MainPageViewModel.kt"
    var isProcessing = false
    var tradingRecordDataReady = false
    var bidAskDataReady = false


    private fun clearStockPanel() {
        stockCurrentPrice.postValue("")
        stockCurrentTick.postValue("")
        stockInfoName.postValue("")
        stockInfoID.postValue("")
        displayTradingRecordList.postValue(arrayListOf())
        stockBidAsk.postValue(
            StockPriceBidAsk(
                arrayListOf(),
                arrayListOf(),
                arrayListOf(),
                arrayListOf(),
                "",
                "",
                "",
                0.0
            )
        )
    }


    fun onClickDownloadBtn() {
        clearStockPanel()
        disposeAllPolling()

        if (!isProcessing) {
            isProcessing = true
            val stockID = stockId.value.toString()
            if (stockID.matches(Regex("[0-9]{4}"))) {
                fetchStockInfo(stockID)
                fetchStockPriceInfo(stockID, Utility.getDaysAgo(1))
            } else {
                toastMessage.postValue("請輸入股票代號")
            }
            isProcessing = false
        }
    }

    private fun disposeAllPolling() {
        if (stockPriceBidAskPolling != null && !stockPriceBidAskPolling!!.isDisposed) {
            stockPriceBidAskPolling!!.dispose()
        }
        if (stockPriceBidAskPolling != null && !tradingRecordPolling!!.isDisposed) {
            tradingRecordPolling!!.dispose()
        }
    }

    private suspend fun fetchStockPriceBidAsk(stockID: String, yesterdayClosePrice: Double) {
        bidAskDataReady = false
            isViewLoading.postValue(true)
            val topFiveOrderRelation =
                StockDataManager.getInstance(getApplication()).getTopFiveOrderRelation(stockID)

            if (topFiveOrderRelation.size == 0) {
                tempBidAsk = DownloadManager()
                    .downloadStockBidAsk(stockID, yesterdayClosePrice)

                StockDataManager.getInstance(getApplication()).saveStockTopFiveOrderData(tempBidAsk)
            } else {
                tempBidAsk.clear()
                for (data in topFiveOrderRelation) {
                    val bidPrice: ArrayList<Double> = arrayListOf()
                    val bidVolume: ArrayList<Int> = arrayListOf()
                    val askPrice: ArrayList<Double> = arrayListOf()
                    val askVolume: ArrayList<Int> = arrayListOf()
                    for (orderData in data.topFiveOrderData) {
                        if (orderData.isBidData) {
                            if (orderData.isPriceData) {
                                bidPrice.add(orderData.value)
                            } else {
                                bidVolume.add(orderData.value.toInt())
                            }
                        } else {
                            if (orderData.isPriceData) {
                                askPrice.add(orderData.value)
                            } else {
                                askVolume.add(orderData.value.toInt())
                            }
                        }
                    }

                    tempBidAsk.add(
                        StockPriceBidAsk(
                            askPrice.toList(),
                            askVolume,
                            bidPrice,
                            bidVolume,
                            data.stockTopFiveOrder.Time,
                            data.stockTopFiveOrder.date,
                            data.stockTopFiveOrder.stockId,
                            data.stockTopFiveOrder.yesterdayPrice
                        )
                    )
                }
            }
            bidAskDataReady = true
            startOpening()
    }

    private fun fetchStockInfo(stockID: String) {
        viewModelScope.launch {
            isViewLoading.postValue(true)
            val request = RetrofitClient.createRetrofit(GeneralConstants.BASE_URL)
                .create(GetRequest::class.java)

            val response = request.getStockInfo(
                DataSetConstants.TAIWAN_STOCK_INFO
                , stockID
            ).awaitResponse()
            Log.d(TAG, "Response Code: ${response.code()}")
            if (response.isSuccessful) {
                Log.d(TAG, "Download Successfully!")
                if (response.body() == null) {
                    Log.d(TAG, "Download empty body!")
                } else {
                    val myData: BaseStockInfo = response.body()!!
                    for (stockInfo in myData.stockInfo) {
                        Log.e(TAG, "" + stockInfo.stock_name)
                        stockInfoID.postValue(stockInfo.stock_id)
                        stockInfoName.postValue(stockInfo.stock_name)
                    }
                }
            }
        }
    }

    private fun fetchStockPriceInfo(stockID: String, date: String) {
        viewModelScope.launch {
            isViewLoading.postValue(true)
            val request = RetrofitClient.createRetrofit(GeneralConstants.BASE_URL)
                .create(GetRequest::class.java)

            val response = request.getStockPriceInfo(
                DataSetConstants.TAIWAN_STOCK_PRICE
                , stockID, date, date
            ).awaitResponse()
            Log.d(TAG, "Response Code: ${response.code()}")
            if (response.isSuccessful) {
                Log.d(TAG, "Download Successfully!")
                if (response.body() == null) {
                    Log.d(TAG, "Download empty body!")
                } else {
                    val myData: BaseStockPriceInfo = response.body()!!
                    for (stockInfo in myData.stockPriceInfo) {
                        Log.e(TAG, "" + stockInfo.close)
                    }
                    if (myData.stockPriceInfo.isNotEmpty()) {
                        fetchTradingRecordData(stockID, myData.stockPriceInfo[0].close)
                        fetchStockPriceBidAsk(stockID, myData.stockPriceInfo[0].close)
                    } else {
                        Log.e(TAG, "Download stockPriceInfo failed!!")
                        isViewLoading.postValue(false)
                    }
                }
            }
        }
    }

    private suspend fun fetchTradingRecordData(stockID: String, yesterdayClosePrice: Double) {
        tradingRecordDataReady = false
            isViewLoading.postValue(true)

            tempTradingRecord =
                StockDataManager.getInstance(getApplication()).getTradingRecords(stockID)

            if (tempTradingRecord.size == 0) {
                tempTradingRecord = DownloadManager()
                    .downloadTradingRecord(stockID, yesterdayClosePrice)
                StockDataManager.getInstance(getApplication()).saveTradingRecords(tempTradingRecord)
            }
            tradingRecordDataReady = true
            startOpening()

    }

    private fun startOpening() {
        synchronized(this) {
            if (tradingRecordDataReady && bidAskDataReady) {
                tradingRecordDataReady = false
                bidAskDataReady = false
                startPoppingTradingRecords()
                startPoppingStockBidAsk()
            }
        }
    }
    private fun startPoppingTradingRecords() {
        isViewLoading.postValue(false)

        val tradingRecordQueue: Queue<TradingRecord> = LinkedList<TradingRecord>(tempTradingRecord)
        val currentDisplay = LinkedList<TradingRecord>()
        tradingRecordPolling = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    while (Utility.compareTimeLessOrEqualThan(
                            tradingRecordQueue.peek()!!.time,
                            Utility.getTimeSince9AM(it.toInt())
                        )
                    ) {
                        currentDisplay!!.addFirst(tradingRecordQueue.poll())
                        if (tradingRecordQueue.peek() == null) {
                            Log.e(TAG, "All trading records popping completed")
                            break
                        }
                    }

                    displayTradingRecordList.postValue(ArrayList(currentDisplay))

                },
                onError = {
                    Log.e(TAG, "Error in popping trading records")
                    Log.e(TAG, "$it.message")
                }
            )
    }

    private fun startPoppingStockBidAsk() {
        isViewLoading.postValue(false)

        val bidAskQueue: Queue<StockPriceBidAsk> = LinkedList<StockPriceBidAsk>(tempBidAsk)
        var currentDisplay: StockPriceBidAsk? = null
        stockPriceBidAskPolling = Observable.interval(1, TimeUnit.SECONDS)
            .subscribeOn(Schedulers.io())
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeBy(
                onNext = {
                    while (Utility.compareTimeLessOrEqualThan(
                            bidAskQueue.peek()!!.Time,
                            Utility.getTimeSince9AM(it.toInt())
                        )
                    ) {
                        currentDisplay = bidAskQueue.poll()
                        if (bidAskQueue.peek() == null) {
                            Log.e(TAG, "All trading records popping completed")
                            break
                        }
                    }

//                    stockBidAsk.postValue(bidAskQueue.poll())
                    if (currentDisplay != null) {
                        stockBidAsk.postValue(currentDisplay)
                    }

                },
                onError = {
                    Log.e(TAG, "Error in popping bid ask data!")
                    Log.e(TAG, "${it.message}")
                }
            )
    }

    override fun onCleared() {
        super.onCleared()
        disposeAllPolling()
    }
}